//OBJECTS AND ARRAY

//numberOfDuplicates
//function numberOfDuplicates(uniqueCount){
//     let newArr =[]
//     let obj={}
//     uniqueCount.forEach(function(el) {
//         obj[el] = (obj[el]||0) + 1;
//         newArr.push(obj[el])
//     });
// return newArr
// }
// console.log(numberOfDuplicates([1, 2, 1, 1, 3])) // [1, 1, 2, 3, 1]
// console.log(numberOfDuplicates(['a', 'a', 'aa', 'a', 'aa']))// [1, 2, 1, 3, 2]

//countObjectStrength
// function countObjectStrength (obj){
//     const weightObject = {
//         undefined: 0,
//         boolean: 1,
//         number: 2,
//         string: 3,
//         object: 5,
//         function: 7,
//         bigint: 9,
//         symbol: 10
//     }
//     let sum = 0;
//     const keys = Object.keys(obj);
//     if (obj.length===0){
//         sum+=weightObject["number"]
//         return sum
//     }
//     if (obj===null){
//         return  console.error("this is a null")
//     }
//     if (typeof obj!=="object"){
//         return console.error("this is not object")
//     }
//     for(let key of keys) {
//         const typeofValue = typeof obj[key];
//         sum = sum + weightObject[typeofValue];
//     }
//     return sum;
// }
// //console.log(countObjectStrength(Array) );// 31 (2 + 3 + 5 + 7 + 7 + 7)
// //console.log(countObjectStrength(Array.prototype) );// 226 (2 + 7 * 32)
// console.log(countObjectStrength([]) );// 2
// console.log(countObjectStrength({some: 'satr'}) );// 3
